const svg = document.querySelector('svg')
const { width, height, x, y } = svg.getBoundingClientRect()
const cursor = document.querySelector('svg .cursor')

let isDown = false
const getMousePart = (x, y): 0 | 1 | 2 => {
  if (x >= 0 && x <= width / 2 && y >= 0 && y <= height / 2) return 0
  if (x > width / 2 && x <= width && y >= 0 && y <= height / 2) return 1
  if (x >= 0 && x <= width && y > height / 2 && y <= height) return 2
}

const transformOrigin = (x, y): { xOrigin: number, yOrigin: number } => {
  const yOrigin = (y - 180) * -1

  if (getMousePart(x, y) === 0) {
    return {
      xOrigin: x - 180,
      yOrigin,
    }
  }

  if (getMousePart(x, y) === 1) {
    return {
      xOrigin: x - 720,
      yOrigin,
    }
  }

  return {
    xOrigin: x - width / 2,
    yOrigin,
  }
}

cursor.addEventListener('mousedown', () => {
  isDown = true
})
window.addEventListener('mouseup', () => {
  isDown = false
})

svg.addEventListener('mousemove', (e) => {
  if (!isDown) return
  const mouseX = e.clientX - x
  const mouseY = e.clientY - y

  const { xOrigin, yOrigin } = transformOrigin(mouseX, mouseY)
  console.log(xOrigin, yOrigin, Math.atan2(yOrigin, xOrigin))
  if (getMousePart(mouseX, mouseY) === 0) {
    (cursor as SVGPathElement).style.transform = `rotate(${(Math.atan2(yOrigin, xOrigin) + Math.PI) * -1}rad)`
  }

  if (getMousePart(mouseX, mouseY) === 1) {
    (cursor as SVGPathElement).style.transform = `translateX(540px) rotate(${(Math.atan2(yOrigin, xOrigin) + Math.PI) * -1}rad)`
  }

  if (getMousePart(mouseX, mouseY) === 2) {
    (cursor as SVGPathElement).style.transform = `translateX(270px) rotate(${(Math.atan2(yOrigin, xOrigin) + Math.PI) * -1}rad)`
  }
})